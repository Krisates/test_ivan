<?php

$inp = array('','PHP','JSON','XML');
$group = array('all'=>'','europe'=>'europe', 'world'=>'world');
$fields = array('unsorted'=>'','region'=>'sregion','code'=>'scode','name'=>'sname','price'=>'sprice');
$sor=array('ASC'=>'','DEC'=>'dec');


$out = '<html><head><meta charset="utf-8"><title>test</title>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js.js"></script>
</head>
<body>
<div>
<select id="filter1">';
foreach ($inp as $in)
	{ $out.='<option value="'.$in.'">'.$in.'</option>';}
$out .= '</select></div>
<div><select id="filter2">';
foreach ($group as $in=>$iv)
	{ $out.='<option value="'.$iv.'">'.$in.'</option>';}
$out .= '</select></div>
<div id="filter3">';
foreach ($fields as  $in=>$iv)
	{ if ($iv!='') {$out.='<input type="checkbox" checked="checked" class="field" id="d'.$in.'" value="'.$in.'">'.$in;}}
$out .= '
</div>
<div>Sort by<select id="filter4">';
foreach ($fields as $in=>$iv)
	{ $out.='<option value="'.$iv.'" id="'.$iv.'">'.$in.'</option>';}
$out .= '</select>
</div>
<div>Sort direction <select id="filter5">';
foreach ($sor as $in=>$iv)
	{ $out.='<option value="'.$iv.'">'.$in.'</option>';}
$out .= '</select>
</div>
<div id="filter6b"></div><div id="filter7b"></div>';

$out.='<div id="res"></div>';

$out.='</body>
</html>';

echo $out;

?>
