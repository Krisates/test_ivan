<?php

class classCurrency
	{
		public $name;
		public $price;
		public $code;
		public $region;
		
		function __construct($code, $name, $price, $region) 
			{
				$this->name = $name;
				$this->price = $price;
				$this->code = $code;
				$this->region = $region;				
			}		
			
		public function getName() 
			{
				return $this->name;
			}
		 
		public function getCode() 
			{
				return $this->code;
			}				
			
		public function getRegion() 
			{
				return $this->region;
			}	
						
		public function getPrice() 
			{
				return $this->price;
			}							
	
		public static function fCode($val1, $val2)
			{
				return strcmp($val1->getCode(), $val2->getCode());
			}
			
		public static function fName($val1, $val2)
			{
				return strcmp($val1->getName(), $val2->getName());
			}		
					
		public static function fRegion($val1, $val2)
			{
				return strcmp($val1->getRegion(), $val2->getRegion());
			}					
			
		public static function fPrice($val1, $val2)
			{
				if ($val1->getPrice()>$val2->getPrice()) {return 1;}
					else {return -1;}
			}								
	}
	

class classBase
	{
		public $currencies = array();
		public $out='';
		
		function cnull()
			{
				$this->currencies = NULL;
			}
					
		function addcur($cur)
			{
				$this->currencies[] = $cur;
			}
			
		function loadinfo($fn)
			{
				if (file_exists($fn) && is_readable ($fn)) 
					{
						$ext = end(explode(".", $fn));
						
						switch ($ext) 
							{
								case 'php':
									$cur = include($fn);
									foreach ($cur as $reg=>$cr)
										{ 
											foreach ($cr as $code=>$curr)
												{
													$this->addcur(new classCurrency( (string) $code, (string) $curr["name"],(float) $curr["value"], (string) $reg));											
												}
										}		
							//		var_dump($this->currencies);	
									break;  								
								case 'json':
									$cur = json_decode(file_get_contents($fn));
									foreach ($cur as $code)
										{
											$this->addcur(new classCurrency( (string) $code[0], (string) $code[1],(float) $code[2], (string) $code[3]));											
										}
							//		var_dump($this->currencies);
									break;  
								case 'xml':
									$cur = simplexml_load_string(file_get_contents($fn));
									foreach ($cur->Item as $Item) 
										{
											$at = $Item->attributes();
											$this->addcur(new classCurrency( (string) $Item->Code, (string) $Item->Description,(float) $Item->Value, (string) $at["Type"]));
										}
							//		var_dump($this->currencies);
									break; 
																			
							}		
				}
			}
			
	

	}


class classFilter extends classBase
	{
		public $data;
		public $dname = true;
		public $dprice = true;
		public $dcode = true;
		public $dregion = true;
		public $vname;
		public $vprice;
		public $vcode;
		public $vregion;
		public $likeop='';
		public $rev;				
		
		function reset_filter()
			{
				$this->data = $this->currencies;
			}

		function filter($tag)
			{
				uasort($this->data, 'classCurrency::f'.$tag);
			}
		
		function revert()
			{
				$this->data = array_reverse($this->data);
			}

		function disp($curr)
			{ 
				$this->out.='<tr class="currency '.$curr->region.'" id="'.$curr->code.'">';
				if ($this->dregion=='true') {$this->out.='<td class="cregion">'.$curr->region.'</td>';}		
				if ($this->dcode=='true') {$this->out.='<td class="ccode">'.$curr->code.'</td>';}						
				if ($this->dname=='true') {$this->out.='<td class="cname">'.$curr->name.'</td>';}
				if ($this->dprice=='true') {$this->out.='<td class="cprice">'.$curr->price.'</td>';}
				$this->out.='</tr>';
			}			
			
		function display()
			{
				$this->out.='<table class="tcur" border="1"><tr>';
				if ($this->dregion=='true') {$this->out.='<th>Region</th>';}	
				if ($this->dcode=='true') {$this->out.='<th>Code</th>';}								
				if ($this->dname=='true') {$this->out.='<th>Name</th>';}
				if ($this->dprice=='true') {$this->out.='<th>Price</th>';}

											
				$this->out.='</tr>';			
				foreach($this->data as $curr)
					{
						switch ($this->likeop)
							{
								case '>': if (!(($this->vprice)&&($curr->price<=$this->vprice))) {$this->disp($curr);}
								break;
								case '=': if (!(($this->vprice)&&($curr->price!=$this->vprice))) {$this->disp($curr);}
								break;
								case '<': if (!(($this->vprice)&&($curr->price>=$this->vprice))) {$this->disp($curr);}
								break;
								case 'ilike': if ((((($this->vregion)&&(strpos($curr->region,$this->vregion))!==false)||($this->vregion==false))&&((($this->vname)&&(strpos($curr->name,$this->vname))!==false)||($this->vname==false)))&&((($this->vcode)&&(strpos(strtolower($curr->code),$this->vcode))!==false)||($this->vcode==false))) {$this->disp($curr);}
								break;
								break;
								case 'ilike>': if (((((($this->vregion)&&(strpos($curr->region,$this->vregion))!==false)||($this->vregion==false))&&((($this->vname)&&(strpos($curr->name,$this->vname))!==false)||($this->vname==false)))&&((($this->vcode)&&(strpos(strtolower($curr->code),$this->vcode))!==false)||($this->vcode==false)))&&(!(($this->vprice)&&($curr->price<=$this->vprice)))) {$this->disp($curr);}
								break;
								break;
								case 'ilike=': if (((((($this->vregion)&&(strpos($curr->region,$this->vregion))!==false)||($this->vregion==false))&&((($this->vname)&&(strpos($curr->name,$this->vname))!==false)||($this->vname==false)))&&((($this->vcode)&&(strpos(strtolower($curr->code),$this->vcode))!==false)||($this->vcode==false)))&&(!(($this->vprice)&&($curr->price!=$this->vprice)))) {$this->disp($curr);}
								break;
								break;
								case 'ilike<': if (((((($this->vregion)&&(strpos($curr->region,$this->vregion))!==false)||($this->vregion==false))&&((($this->vname)&&(strpos($curr->name,$this->vname))!==false)||($this->vname==false)))&&((($this->vcode)&&(strpos(strtolower($curr->code),$this->vcode))!==false)||($this->vcode==false)))&&(!(($this->vprice)&&($curr->price>=$this->vprice)))) {$this->disp($curr);}
								break;																								
								case '': $this->disp($curr);
								break;							
							}
					}
				$this->out.='</table>';
			}			
			
								
	}

?>
