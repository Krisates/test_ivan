<?php
require_once('classes.php');

class classBaseTest extends PHPUnit_Framework_TestCase
{

    public function test1()
    {
        $a = new classBase;
        $a->cnull();

        // Assert
        $this->assertEquals(NULL, $a->currencies);
    }


    public function test2()
    {
        $a = new classCurrency('code', 'name', 1, 'region');

        $b=$a->getCode().$a->getName().$a->getPrice().$a->getRegion();

        // Assert
        $this->assertEquals('code'.'name'.'1'.'region', $b);
    }
    
    public function test3()
    {
        $a = new classCurrency('code', 'name', 1, 'region');
        $val1 = new classCurrency('code', 'name', 1, 'region');
        $val2 = new classCurrency('code', 'name', 4, 'region');

        $b=$a->fPrice($val1, $val2);

        // Assert
        $this->assertEquals(-1, $b);
    }
    
    public function test4()
    {
        $a = new classFilter;
        $val1 = new classCurrency('code1', 'name1', 3, 'region');
        $val2 = new classCurrency('code2', 'name2', 1, 'region');
        $val3 = new classCurrency('code3', 'name3', 4, 'region');        
        $this->currencies[] = $val1;
        $this->currencies[] = $val2;
        $this->currencies[] = $val3;               

        $a->reset_filter();

        // Assert
        $this->assertEquals($a->data, $a->currencies);
    }   
    
    public function test5()
    {
        $a = new classFilter;
        $val1 = new classCurrency('code1', 'name1', 3, 'region');
        $val2 = new classCurrency('code2', 'name2', 1, 'region');
        $val3 = new classCurrency('code3', 'name3', 4, 'region');        
        $this->currencies[] = $val1;
        $this->currencies[] = $val2;
        $this->currencies[] = $val3;   
        $a->reset_filter();       
		
		$b = $a->data;
        $c = $a->revert();
        $b = array_reverse($b);

        // Assert
        $this->assertEquals($a->data, $b);
    }   
    
}

?>
